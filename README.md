# Flutter App Builder Image

The Docker image that powers the CI/CD process of Glutyfree.

Simply run a new pipeline and set FLUTTER_VERSION to stable or beta (can be any git reference in the flutter git repo).
