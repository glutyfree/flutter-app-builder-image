FROM ubuntu

WORKDIR /app
ENV ANDROID_HOME /opt/android-sdk
ENV ANDROID_SDK_TOOLS /opt/android-sdk-tools

RUN apt-get update \
    && apt-get -y install git curl unzip libglu1-mesa wget xz-utils lib32stdc++6 openjdk-8-jdk-headless \
    && rm -rf /var/lib/apt/lists/*

# clone and install flutter
ARG FLUTTER_VERSION=beta
ENV FLUTTER_VERSION=$FLUTTER_VERSION
RUN git clone https://github.com/flutter/flutter.git -b $FLUTTER_VERSION /opt/flutter
ENV PATH=${PATH}:/opt/flutter/bin
RUN flutter --version

# install android tools
ENV PATH ${PATH}:${ANDROID_SDK_TOOLS}/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/bin/
RUN wget -q https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O /tmp/android-sdk.zip && \
    unzip -q /tmp/android-sdk.zip -d /tmp && \
    mv /tmp/tools ${ANDROID_SDK_TOOLS} && \
    rm /tmp/android-sdk.zip
# mute stdout of sdkmanager licenses
RUN yes | sdkmanager --licenses > /dev/null
# mute stdout of sdkmanager since it creates that much noise that gitlab logs limit gets exceeded
RUN yes | sdkmanager --sdk_root=${ANDROID_HOME} --install "build-tools;27.0.3" "extras;android;m2repository" "extras;google;m2repository" "patcher;v4" platform-tools "platforms;android-27" tools > /dev/null
RUN sdkmanager --list
# we need java for the android build
RUN apt-get update && apt-get install -y openjdk-8-jdk-headless && rm -rf /var/lib/apt/lists/*

# show fluctor doctor output first after android-sdk is installed
RUN flutter doctor
